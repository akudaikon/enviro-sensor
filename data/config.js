getSettings = function()
{
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) {
      if (xhttp.status == 200) {
        var settings = JSON.parse(xhttp.responseText);
        document.getElementById('sensorName').value = settings.sensorName;
        document.getElementById('mdnsName').value = settings.mdnsName;
        document.getElementById('mqttServer').value = settings.mqttServer;
        document.getElementById('mqttUser').value = settings.mqttUser;
        document.getElementById('mqttPassword').value = settings.mqttPassword;
        document.getElementById('updateRate').value = settings.updateRate;
        document.getElementById('tempOffset').value = settings.tempOffset;
        document.getElementById('haDiscovery').checked = settings.haDiscovery;
        document.getElementById('aqiLEDBrightness').value = settings.aqiLEDBrightness;
        updateTopics(settings.sensorName);
      }
    }
  };
  xhttp.open("GET", "getsettings", true);
  xhttp.send();
}

updateTopics = function(value)
{
  document.getElementById('mqttTemperatureTopic').innerHTML = "<h6>enviro/" + value + "/temperature</h6>";
  document.getElementById('mqttHumidityTopic').innerHTML = "<h6>enviro/" + value + "/humidity</h6>";
  document.getElementById('mqttPressureTopic').innerHTML = "<h6>enviro/" + value + "/pressure</h6>";
  document.getElementById('mqttPM25Topic').innerHTML = "<h6>enviro/" + value + "/PM2.5</h6>";
  document.getElementById('mqttPM10Topic').innerHTML = "<h6>enviro/" + value + "/PM10</h6>";
  document.getElementById('mqttCO2Topic').innerHTML = "<h6>enviro/" + value + "/CO2</h6>";
  document.getElementById('mqttVOCTopic').innerHTML = "<h6>enviro/" + value + "/VOC</h6>";
  document.getElementById('mqttAQIValueTopic').innerHTML = "<h6>enviro/" + value + "/AQI/value</h6>";
  document.getElementById('mqttAQITextTopic').innerHTML = "<h6>enviro/" + value + "/AQI/text</h6>";
  document.getElementById('mqttPollutantTopic').innerHTML = "<h6>enviro/" + value + "/AQI/pollutant</h6>";
  document.getElementById('mqttAvailabilityTopic').innerHTML = "<h6>enviro/" + value + "/availability</h6>";
  document.getElementById('mqttDisplayCommandTopic').innerHTML = "<h6>enviro/" + value + "/display/command</h6>";
  document.getElementById('mqttDisplayStateTopic').innerHTML = "<h6>enviro/" + value + "/display/state</h6>";
}

// To get around onLoad not firing on back
setTimeout(getSettings, 100);