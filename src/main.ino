// SDS011 uses Serial with pin swap

// Wemos D1 Mini Pinout
// Neopixel - RX (GPIO 3)
// I2C SCL - D1 (GPIO 5)
// I2C SDA - D2 (GPIO 4)
// SDS011 TX - D7 (GPIO 13)
// SDS011 RX - D8 (GPIO 15)

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServerSPIFFS.h>
#include <WiFiManager.h>
#include <DNSServer.h>
#include <WiFiClient.h>
#include <PubSubClient.h>
#include <EEPROM.h>
#include <FS.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_I2CDevice.h>
#include <NeoPixelBrightnessBus.h>
#include <SparkLine.h>
#include <SDS011.h>
#include <ArduinoJson.h>
#include "bsec.h"
#include "bsec_serialized_configurations_iaq.h"
#include "icons.h"

#define AQI_LED_PIN         RX

#define I2C_SSD1306_ADDR    0x3C
#define I2C_T6713_ADDR      0x15

#define DISPLAY_UPDATE_RATE (5 * 1000)   // 5 seconds (in ms)
#define SDS011_UPDATE_RATE  5            // 5 minutes

#define AQI_AVG_NUM_SAMPLES 20           // dictated by weight average equation

#define SETTINGS_REV        0xA4
#define MAX_STRING_LENGTH   128

#define BSEC_STATE_SAVE_PERIOD   UINT32_C(360 * 60 * 1000) // 360 minutes - 4 times a day

enum EEPROMSettings
{
  SETTING_INITIALIZED,
  SETTING_UPDATE_RATE,
  SETTING_HA_DISCOVERY,
  SETTING_AQILED_BRIGHTNESS,
  SETTING_TEMP_OFFSET,
  SETTING_DELIM = SETTING_TEMP_OFFSET + 4,  // SETTING_TEMP_OFFSET is a float
  // Max 128 characters for EEPROM stored strings below...
  SETTING_NAME_LENGTH,
  SETTING_NAME,
  SETTING_MDNS_LENGTH = SETTING_NAME + MAX_STRING_LENGTH,
  SETTING_MDNS,
  SETTING_MQTT_SERVER_LENGTH = SETTING_MDNS + MAX_STRING_LENGTH,
  SETTING_MQTT_SERVER,
  SETTING_MQTT_USER_LENGTH = SETTING_MQTT_SERVER + MAX_STRING_LENGTH,
  SETTING_MQTT_USER,
  SETTING_MQTT_PASSWORD_LENGTH = SETTING_MQTT_USER + MAX_STRING_LENGTH,
  SETTING_MQTT_PASSWORD,
  SETTING_BSEC_DATA = SETTING_MQTT_PASSWORD + MAX_STRING_LENGTH,
  NUM_OF_SETTINGS = SETTING_BSEC_DATA + BSEC_MAX_STATE_BLOB_SIZE,
};

enum displayState
{
  DISPLAY_ERROR,
  DISPLAY_TEMP_HUMID,
  DISPLAY_PM,
  DISPLAY_CO2_VOC,
  DISPLAY_AQI,
  DISPLAY_NUM_STATES
};

enum AQILevel
{
  AQI_NA,
  AQI_GOOD,
  AQI_MODERATE,
  AQI_SLIGHTLY_UNHEALTHY,
  AQI_UNHEALTHY,
  AQI_VERY_UNHEALTHY,
  AQI_HAZARDOUS
};

enum AQIPollutant
{
  AQI_POLLUTANT_NA,
  AQI_POLLUTANT_PM25,
  AQI_POLLUTANT_PM10,
  AQI_POLLUTANT_VOC,
  AQI_POLLUTANT_CO2
};

uint32_t AQIColors[] = { 0x0F0F0F, 0x00FF00, 0xFFA500, 0xFF4500, 0xFF0000, 0x4B004B, 0x200005 };

MDNSResponder mdns;
WiFiClient mqttClient;
PubSubClient mqtt(mqttClient);
ESP8266WebServer server(80);
ESP8266HTTPUpdateServer httpUpdater;

Bsec bmeSensor;
uint8_t bsecState[BSEC_MAX_STATE_BLOB_SIZE] = {0};
uint16_t stateUpdateCounter = 0;

Adafruit_SSD1306 display(128, 64, &Wire, -1);
SDS011 sds011;

NeoPixelBrightnessBus<NeoGrbFeature, Neo800KbpsMethod> aqiLED(1, AQI_LED_PIN);

char apName[14];
String sensorName;
String mdnsName;
String mqttServer;
String mqttUser;
String mqttPassword;
uint8_t updateRate;
uint8_t aqiLEDBrightness = 100;
bool haDiscovery = false;
float tempOffset;

bool displayEnabled = true;
bool mqttUpdateFlag = false;
uint8_t displayState = DISPLAY_ERROR;
uint32_t lastBMEUpdate = 0;
uint32_t last5SecTick = 0;
uint32_t lastMQTTUpdate = 0;
uint32_t lastSparkLineUpdate = 0;
uint32_t lastRestart = millis();

float temperature = -1;
float temperatureC = -1;
float humidity = -1;
float pressure = -1;
float PM25 = -1;
float PM10 = -1;
uint16_t CO2 = UINT16_MAX;
uint16_t VOC = UINT16_MAX;

uint8_t AQI = AQI_NA;
uint16_t AQIvalue = 0;
uint16_t AQIvalue_samples[AQI_AVG_NUM_SAMPLES] = { 0 };
uint8_t AQIpollutant = AQI_POLLUTANT_NA;

SparkLine<float> temperatureSparkLine(60, [](const uint16_t x0, const uint16_t y0, const uint16_t x1, const uint16_t y1) { 
  display.drawLine(x0, y0, x1, y1, WHITE);
});

SparkLine<float> humiditySparkLine(60, [](const uint16_t x0, const uint16_t y0, const uint16_t x1, const uint16_t y1) { 
  display.drawLine(x0, y0, x1, y1, WHITE);
});

SparkLine<float> PM25SparkLine(60, [](const uint16_t x0, const uint16_t y0, const uint16_t x1, const uint16_t y1) { 
  display.drawLine(x0, y0, x1, y1, WHITE);
});

SparkLine<float> PM10SparkLine(60, [](const uint16_t x0, const uint16_t y0, const uint16_t x1, const uint16_t y1) { 
  display.drawLine(x0, y0, x1, y1, WHITE);
});

SparkLine<uint16_t> CO2SparkLine(60, [](const uint16_t x0, const uint16_t y0, const uint16_t x1, const uint16_t y1) { 
  display.drawLine(x0, y0, x1, y1, WHITE);
});

SparkLine<uint16_t> VOCSparkLine(60, [](const uint16_t x0, const uint16_t y0, const uint16_t x1, const uint16_t y1) { 
  display.drawLine(x0, y0, x1, y1, WHITE);
});

void wifi_configModeCallback(WiFiManager *myWiFiManager)
{
  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setTextSize(2);
  display.setCursor(60, 5);
  display.print("!");
  display.setTextSize(1);
  display.setCursor(39, 25);
  display.print(F("Connect to"));
  display.setTextSize(2);
  display.setCursor(0, 35);
  display.print(apName);
  display.setTextSize(1);
  display.setCursor(15, 50);
  display.print(F("to configure Wifi"));
  display.display();
}

void display_firmwareUpdateStart()
{
  display.clearDisplay();
  display.setTextSize(2);
  display.setCursor(50, 12);
  display.print("...");
  display.setTextSize(1);
  display.setCursor(15, 40);
  display.println(F("Updating Firmware"));
  display.display();
}

void display_firmwareUpdateSuccess()
{
  display.clearDisplay();

  // draw lazy checkmark
  display.setTextSize(2);
  display.setCursor(57, 10);
  display.print("\\");
  display.setCursor(65, 10);
  display.print("/");
  display.fillRect(57, 10, 4, 16, BLACK);

  display.setTextSize(1);
  display.setCursor(21, 35);
  display.print(F("Firmware Update"));
  display.setCursor(31, 50);
  display.print(F("Successful!"));
  display.display();
}

void display_firmwareUpdateFail()
{
  display.setTextSize(2);
  display.setCursor(60, 10);
  display.print("!");
  display.setTextSize(1);
  display.setCursor(21, 35);
  display.print(F("Firmware Update"));
  display.setCursor(44, 50);
  display.print(F("Failed"));
  display.display();
}

void setup()
{
  Serial.begin(9600);
  Serial.swap();
  Wire.begin();

  // Get MAC address and build AP SSID
  uint8_t macAddr[6];
  WiFi.macAddress(macAddr);
  sprintf(apName, "Env-%02x%02x%02x", macAddr[3], macAddr[4], macAddr[5]);
  mdnsName = apName;

  // Initialize and read EEPROM settings
  EEPROM.begin(NUM_OF_SETTINGS);
  EEPROM_readSettings();

  // Initialize SPIFFS
  SPIFFS.begin();

  // Initialize AQI LED
  aqiLED.Begin();
  aqiLED.SetPixelColor(0, HtmlColor(AQIColors[AQI_NA]));
  aqiLED.Show();

  // Initialize display
  display.begin(SSD1306_SWITCHCAPVCC, I2C_SSD1306_ADDR);
  display.ssd1306_command(SSD1306_SETCONTRAST);
  display.ssd1306_command(0x20);
  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setTextSize(2);
  display.setCursor(50, 12);
  display.print("...");
  display.setTextSize(1);
  display.setCursor(12, 40);
  display.println(F("Connecting to Wifi"));
  display.display();

  // Connect to WiFi
  WiFiManager wifiManager;
  wifiManager.setTimeout(180);  // 3 minutes
  wifiManager.setAPCallback(wifi_configModeCallback);
  if (!wifiManager.autoConnect(apName))
  {
    display.setTextSize(2);
    display.setCursor(60, 0);
    display.print("!");
    display.setCursor(5, 20);
    display.setTextSize(1);
    display.print(F("Unable to connect to        WiFi!"));
    display.setCursor(0, 45);
    display.print(F("Check WiFi and sensor        settings"));
    delay(3000);
    ESP.reset();
  }

  // Initialize BME680
  bmeSensor.begin(BME680_I2C_ADDR_SECONDARY, Wire);
  bmeSensor.setConfig(bsec_config_iaq); // generic_33v_3s_4d
  bmeSensor.setTemperatureOffset(tempOffset);
  BME680_loadState();

  bsec_virtual_sensor_t sensorList[] = {
    BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_TEMPERATURE,
    BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_HUMIDITY,
    BSEC_OUTPUT_RAW_PRESSURE,
    BSEC_OUTPUT_IAQ,
    BSEC_OUTPUT_STATIC_IAQ,
  };
  bmeSensor.updateSubscription(sensorList, 5, BSEC_SAMPLE_RATE_LP);

  // Initialize SDS011 dust sensor
  sds011.setup(&Serial);
  sds011.onData([](float pm25Value, float pm10Value)
  {
    PM25 = pm25Value;
    PM10 = pm10Value;
    calculateAQI();
  });
  sds011.setReportMode(false); // active mode
  sds011.setWorkingPeriod(SDS011_UPDATE_RATE);

  // Enable T6713 ABC Logic
  T6173_enableABCLogic();

  // Start servers
  mdns.begin(mdnsName.c_str());
  server.on("/", HTTP_GET, []() { handleFileRead("/config.htm"); });
  server.on("/settings", HTTP_POST, settings_save);
  server.on("/getsettings", settings_get);
  server.on("/debug", []()
  {
    DynamicJsonDocument root(256);

    root["temperatureF"] = temperature;
    root["temperatureC"] = temperatureC;
    root["humidity"] = humidity;
    root["pressure"] = pressure;
    root["PM2.5"] = PM25;
    root["PM10"] = PM10;
    root["CO2"] = CO2;
    root["sVOC"] = VOC;
    root["sVOC accuracy"] = bmeSensor.staticIaqAccuracy;
    root["AQI"] = AQIvalue;
    root["AQI text"] = AQI;
    root["AQI pollutant"] = AQIpollutant;

    String buffer;
    serializeJson(root, buffer);

    server.sendHeader("Cache-Control", "no-cache");
    server.send(200, "text/json", buffer);
  });
  server.on("/reset", [&wifiManager]()
  {
    wifiManager.resetSettings();
    server.send(200, "text/plain", F("Clearing all saved settings (including Wifi) and restarting..."));
    server.handleClient();
    delay(2000);
    ESP.restart();
  });
  server.on("/restart", []()
  {
    server.send(200, "text/html", F("<META http-equiv=\"refresh\" content=\"10;URL='/'\">Restarting..."));
    server.handleClient();
    delay(2000);
    ESP.restart();
  });
  server.onNotFound([]()
  {
    if (!handleFileRead(server.uri())) server.send(404, "text/plain", "Not Found");
  });
  httpUpdater.setup(&server);
  httpUpdater.setStartCallback(display_firmwareUpdateStart);
  httpUpdater.setSuccessCallback(display_firmwareUpdateSuccess);
  httpUpdater.setFailCallback(display_firmwareUpdateFail);
  server.begin();

  // Setup MQTT
  mqtt.setServer(mqttServer.c_str(), 1883);
  mqtt.setCallback(MQTT_callback);
  mqtt.setBufferSize(1024);
  MQTT_reconnect();

  // Show that we're connected
  IPAddress ip = WiFi.localIP();
  display.clearDisplay();

  // draw lazy checkmark
  display.setTextSize(2);
  display.setCursor(57, 5);
  display.print("\\");
  display.setCursor(65, 5);
  display.print("/");
  display.fillRect(57, 5, 4, 11, BLACK);

  display.setTextSize(1);
  display.setCursor(10, 25);
  display.println(F("Connected to Wifi!"));
  display.setCursor(0, 40);
  display.print(String(ip[0])); display.print("."); display.print(String(ip[1])); display.print("."); display.print(String(ip[2])); display.print("."); display.print(String(ip[3]));
  display.setCursor(0, 50);
  display.print(mdnsName);
  display.print(F(".local"));
  display.display();
  delay(3000);
}

void loop()
{
  // BSEC is annoying and NEEDS to run EVERY 3 seconds or else IAQ fails.
  // Therefore this is all laid out to ensure that happens.

  // Guardband to ensure BSEC can do processing every 3 seconds
  // Ensure nothing else can happen within 1.5 seconds of when BSEC processing needs to happen
  if (millis() - lastBMEUpdate > 1500)
  {
    // Get BME680 data
    if (bmeSensor.run())
    {
      lastBMEUpdate = millis();
      temperatureC = bmeSensor.temperature;
      temperature = (bmeSensor.temperature * 1.8) + 32; // to degF
      humidity = bmeSensor.humidity;
      pressure = (bmeSensor.pressure * 0.000295); // to inHg

      if (bmeSensor.staticIaqAccuracy != 0)
      {
        // baseline VOC seems to be 25, so negate that before calculating percent
        VOC = ((constrain(bmeSensor.staticIaq, 25, 475) - 25) / 500) * 100;
      }
      else VOC = UINT16_MAX;
      BME680_updateState();
    }
  }
  else
  {
    // Check for any updates from SDS011
    sds011.loop();

    // Every 5 seconds...
    if (millis() - last5SecTick > DISPLAY_UPDATE_RATE)
    {
      // Get T6713 data
      // Check to make sure T6713 is warmed up before trying to read CO2 data
      if (!(T6713_getStatus() & 0x800)) CO2 = T6713_getData();
      else CO2 = UINT16_MAX;
      if (CO2 == 0) CO2 = UINT16_MAX; // sometimes we get a 0? just make it invalid

      // Update display
      if (displayEnabled)
      {
        if (++displayState >= DISPLAY_NUM_STATES) displayState = DISPLAY_ERROR;
        display_update();
      }
      last5SecTick = millis();
    }

    // Update sparklines every minute
    else if (millis() - lastSparkLineUpdate > (60 * 1000))
    {
      if (temperature != -1) temperatureSparkLine.add(temperature);
      if (humidity != -1) humiditySparkLine.add(humidity);
      if (PM25 != -1) PM25SparkLine.add(PM25);
      if (PM10 != -1) PM10SparkLine.add(PM10);
      if (CO2 != UINT16_MAX) CO2SparkLine.add(CO2);
      if (VOC != UINT16_MAX) VOCSparkLine.add(VOC);
      lastSparkLineUpdate = millis();
    }

    // Send update to MQTT every updateRate
    else if (millis() - lastMQTTUpdate > (updateRate * 60 * 1000))
    {
      mqttUpdateFlag = true;
      lastMQTTUpdate = millis();
    }

    // Restart ESP every 7 days
    // BSEC likes to crash the ESP every few weeks, so we force a restart to fix that
    else if (millis() - lastRestart > (7 * 24 * 60 * 60 * 1000))
    {
      ESP.restart();
    }

    else
    {
      // Handle HTTP
      server.handleClient();

      // Handle MQTT
      if (!mqtt.connected()) MQTT_reconnect();
      if (mqtt.connected())
      {
        if (mqttUpdateFlag)
        {
          MQTT_update();
          mqttUpdateFlag = false;
        }
        mqtt.loop();
      }
    }
  }
}

void T6173_enableABCLogic()
{
  Wire.beginTransmission(I2C_T6713_ADDR);
  Wire.write(0x05); Wire.write(0x03); Wire.write(0xEE); Wire.write(0xFF); Wire.write(0x00);
  Wire.endTransmission();
}

uint16_t T6713_getStatus()
{
  Wire.beginTransmission(I2C_T6713_ADDR);
  Wire.write(0x04); Wire.write(0x13); Wire.write(0x8A); Wire.write(0x00); Wire.write(0x01);
  Wire.endTransmission();

  // Read status register
  Wire.requestFrom(I2C_T6713_ADDR, 4);
  Wire.read();  // first 2 bytes are don't care
  Wire.read();
  return (Wire.read() << 8) | Wire.read();
}

uint16_t T6713_getData()
{
  // Start CO2 measurement
  Wire.beginTransmission(I2C_T6713_ADDR);
  Wire.write(0x04); Wire.write(0x13); Wire.write(0x8B); Wire.write(0x00); Wire.write(0x01);
  Wire.endTransmission();

  // Read CO2 measurement (in ppm)
  delay(2000);
  Wire.requestFrom(I2C_T6713_ADDR, 4);
  Wire.read();  // first 2 bytes are don't care
  Wire.read();
  return ((Wire.read() & 0x3F) << 8) | Wire.read();
}

uint16_t calcAvg(uint16_t newSample, uint16_t samples[], uint8_t numSamples)
{
  for (uint8_t i = numSamples; i > 0; i--) samples[i] = samples[i - 1];
  samples[0] = newSample;

  uint16_t avg = 0;
  for (uint8_t i = 0; i < numSamples; i++) avg += samples[i];
  avg /= numSamples;

  return avg;
}

uint16_t calcWeightedAvg(uint16_t newSample, uint16_t samples[], uint8_t numSamples)
{
  for (uint8_t i = numSamples; i > 0; i--) samples[i] = samples[i - 1];
  samples[0] = newSample;

  double weightedAvg = 0;
  for (uint8_t i = 0; i < numSamples; i++) weightedAvg += (double)(samples[i] * pow(0.5, (i + 1)));

  return (uint16_t)weightedAvg;
}

void calculateAQI()
{
  uint16_t PM25AQI = 0;
  uint16_t PM10AQI = 0;
  uint16_t CO2AQI = 0;
  uint16_t VOCAQI = 0; // (VOC != UINT16_MAX ? (constrain(bmeSensor.staticIaq, 25, 475) - 25) : 0);

  // Calculate AQI for PM2.5
  if (PM25 != -1)
  {
    if (PM25 < 12.0)        PM25AQI = mapFloat(PM25, 0.0, 12.0, 0, 50);
    else if (PM25 < 35.5)   PM25AQI = mapFloat(PM25, 12.0, 35.5, 50, 100);
    else if (PM25 < 55.5)   PM25AQI = mapFloat(PM25, 35.5, 55.5, 100, 150);
    else if (PM25 < 150.5)  PM25AQI = mapFloat(PM25, 55.5, 150.5, 150, 200);
    else if (PM25 < 250.5)  PM25AQI = mapFloat(PM25, 150.5, 250.5, 200, 300);
    else if (PM25 < 350.5)  PM25AQI = mapFloat(PM25, 250.5, 350.5, 300, 400);
    else if (PM25 >= 350.5) PM25AQI = mapFloat(PM25, 350.5, 500.5, 400, 500);
  }

  // Calculate AQI for PM10
  if (PM10 != -1)
  {
    if (PM10 < 55.0)        PM10AQI = mapFloat(PM10, 0.0, 55.0, 0, 50);
    else if (PM10 < 155.0)  PM10AQI = mapFloat(PM10, 55.0, 155.0, 50, 100);
    else if (PM10 < 255.0)  PM10AQI = mapFloat(PM10, 155.0, 255.0, 100, 150);
    else if (PM10 < 355.0)  PM10AQI = mapFloat(PM10, 255.0, 355.0, 150, 200);
    else if (PM10 < 425.0)  PM10AQI = mapFloat(PM10, 355.0, 425.0, 200, 300);
    else if (PM10 < 505.0)  PM10AQI = mapFloat(PM10, 425.0, 505.0, 300, 400);
    else if (PM10 >= 505.0) PM10AQI = mapFloat(PM10, 505.5, 605.0, 400, 500);
  }

  // Calculate AQI for CO2 (not official science since CO2 doesn't fall under EPA AQI)
  if (CO2 != UINT16_MAX)
  {
    if (CO2 < 700)        CO2AQI = mapFloat(CO2, 0, 700, 0, 50);
    else if (CO2 < 1000)  CO2AQI = mapFloat(CO2, 700, 1000, 50, 100);
    else if (CO2 < 1500)  CO2AQI = mapFloat(CO2, 1000, 1500, 100, 150);
    else if (CO2 < 2500)  CO2AQI = mapFloat(CO2, 1500, 2500, 150, 200);
    else if (CO2 < 5000)  CO2AQI = mapFloat(CO2, 2500, 5000, 200, 300);
    else if (CO2 >= 5000) CO2AQI = mapFloat(CO2, 5000, 30000, 300, 500);
  }

  // Find largest AQI value out of PM2.5, PM10, VOC, and CO2
  if (PM25 != -1 && PM25AQI >= PM10AQI && PM25AQI >= VOCAQI && PM25AQI >= CO2AQI)
  {
    AQIpollutant = AQI_POLLUTANT_PM25;
    AQIvalue = PM25AQI;
  }
  else if (PM10 != -1 && PM10AQI >= VOCAQI && PM10AQI >= CO2AQI)
  {
    AQIpollutant = AQI_POLLUTANT_PM10;
    AQIvalue = PM10AQI;
  }
  else if (VOC != UINT16_MAX && VOCAQI >= CO2AQI)
  {
    AQIpollutant = AQI_POLLUTANT_VOC;
    AQIvalue = VOCAQI;
  }
  else if (CO2 != UINT16_MAX)
  {
    AQIpollutant = AQI_POLLUTANT_CO2;
    AQIvalue = CO2AQI;
  }
  else
  {
    AQIpollutant = AQI_POLLUTANT_NA;
    AQIvalue = UINT16_MAX;
  }

  //AQIvalue = calcWeightedAvg(AQIvalue, AQIvalue_samples, AQI_AVG_NUM_SAMPLES);

  // Set AQI rating based on AQI value
  if (AQIvalue != UINT16_MAX)
  {
    if (AQIvalue < 50)        AQI = AQI_GOOD;
    else if (AQIvalue < 100)  AQI = AQI_MODERATE;
    else if (AQIvalue < 150)  AQI = AQI_SLIGHTLY_UNHEALTHY;
    else if (AQIvalue < 200)  AQI = AQI_UNHEALTHY;
    else if (AQIvalue < 300)  AQI = AQI_VERY_UNHEALTHY;
    else if (AQIvalue >= 300) AQI = AQI_HAZARDOUS;
  }
  else AQI = AQI_NA;

  if (displayEnabled)
  {
    if (AQI == AQI_NA) aqiLED.SetBrightness(100);
    else aqiLED.SetBrightness(aqiLEDBrightness);

    aqiLED.SetPixelColor(0, HtmlColor(AQIColors[AQI]));
    aqiLED.Show();
  }
}

void display_update()
{
  char temp[6];
  display.clearDisplay();

  switch (displayState)
  {
    case DISPLAY_ERROR:
      if (!WiFi.isConnected())
      {
        display.setTextSize(2);
        display.setCursor(60, 0);
        display.print("!");
        display.setCursor(5, 20);
        display.setTextSize(1);
        display.print(F("Unable to connect to        WiFi!"));
        display.setCursor(0, 45);
        display.print(F("Check WiFi and sensor        settings"));
        break;
      }
      else if (!mqtt.connected())
      {
        display.setTextSize(2);
        display.setCursor(60, 0);
        display.print("!");
        display.setCursor(5, 20);
        display.setTextSize(1);
        display.print(F("Unable to connect to        MQTT!"));
        display.setCursor(0, 45);
        display.print(F("Check MQTT server and   sensor settings"));
        break;
      }
      displayState++;
    // no break here intentionally

    case DISPLAY_TEMP_HUMID:
      // Temperature
      display.setTextSize(1);
      display.setCursor(0, 7);
      display.print("Temp:");

      display.setTextSize(2);
      display.setCursor(45, 0);
      if (temperature != -1)
      {
        ((temperature < 65 || temperature > 80) ? display.setTextColor(BLACK, WHITE) : display.setTextColor(WHITE));
        dtostrf(temperature, 5, 1, temp);
        display.print(temp);
        display.setTextColor(WHITE);
      }
      else display.print("    -");

      display.setTextSize(1);
      display.setCursor(110, 7);
      display.print((char)247); // degree
      display.print("F");

      temperatureSparkLine.draw(0, 30, 128, 10);

      // Humidity
      display.setCursor(0, 41);
      display.setTextSize(1);
      display.print("Humid:");

      display.setCursor(45, 34);
      display.setTextSize(2);
      if (humidity != -1)
      {
        ((humidity < 30 || humidity > 70) ? display.setTextColor(BLACK, WHITE) : display.setTextColor(WHITE));
        dtostrf(humidity, 5, 1, temp);
        display.print(temp);
        display.setTextColor(WHITE);
      }
      else display.print("    -");

      display.setTextSize(1);
      display.setCursor(110, 41);
      display.print("%");

      humiditySparkLine.draw(0, 62, 128, 10);
    break;

    case DISPLAY_PM:
      // PM2.5
      display.setTextSize(1);
      display.setCursor(0, 7);
      display.print("PM2.5:");

      display.setTextSize(2);
      display.setCursor(45, 0);
      if (PM25 != -1)
      {
        (PM25 > 35.5 ? display.setTextColor(BLACK, WHITE) : display.setTextColor(WHITE));
        dtostrf(PM25, 5, 1, temp);
        display.print(temp);
        display.setTextColor(WHITE);
      }
      else display.print("    -");

      display.setTextSize(1);
      display.setCursor(110, 0);
      display.print((char)229); // mu
      display.print("g");
      display.setCursor(111, 7);
      display.print("/m");
      display.drawBitmap(123, 7, symbol_cubed, 3, 5, WHITE);

      PM25SparkLine.draw(0, 30, 128, 10);

      // PM10
      display.setCursor(0, 41);
      display.setTextSize(1);
      display.print("PM10:");

      display.setCursor(45, 34);
      display.setTextSize(2);
      if (PM10 != -1)
      {
        (PM10 > 155 ? display.setTextColor(BLACK, WHITE) : display.setTextColor(WHITE));
        dtostrf(PM10, 5, 1, temp);
        display.print(temp);
        display.setTextColor(WHITE);
      }
      else display.print("    -");

      display.setTextSize(1);
      display.setCursor(110, 34);
      display.print((char)229); // mu
      display.print("g");
      display.setCursor(111, 41);
      display.print("/m");
      display.drawBitmap(123, 41, symbol_cubed, 3, 5, WHITE); 

      PM10SparkLine.draw(0, 62, 128, 10);
    break;

    case DISPLAY_CO2_VOC:
      // CO2
      display.setCursor(0, 7);
      display.setTextSize(1);
      display.print("CO2:");

      display.setCursor(45, 0);
      display.setTextSize(2);
      if (CO2 != UINT16_MAX)
      {
        (CO2 > 1000 ? display.setTextColor(BLACK, WHITE) : display.setTextColor(WHITE));
        sprintf(temp, "%5d", CO2);
        display.print(temp);
        display.setTextColor(WHITE);
      }
      else display.print("    -");

      display.setTextSize(1);
      display.setCursor(110, 7);
      display.print("ppm");

      CO2SparkLine.draw(0, 30, 128, 10);

      // VOC
      display.setCursor(0, 41);
      display.setTextSize(1);
      display.print("VOC:");

      display.setCursor(45, 34);
      display.setTextSize(2);
      if (VOC != UINT16_MAX)
      {
        (VOC > 20 ? display.setTextColor(BLACK, WHITE) : display.setTextColor(WHITE));
        sprintf(temp, "%5d", VOC);
        display.print(temp);
        display.setTextColor(WHITE);
      }
      else display.print("    -");

      display.setTextSize(1);
      display.setCursor(110, 41);
      display.print("%");

      VOCSparkLine.draw(0, 62, 128, 10);
    break;

    case DISPLAY_AQI:
      display.setTextSize(2);

      switch (AQI)
      {
        case AQI_NA:
          display.setCursor(49, 29);
          display.print("N/A");
        break;

        case AQI_GOOD:
          display.drawBitmap(48, 0, AQIicon_healthy, 32, 32, WHITE);
          display.setCursor(41, 42);
          display.print("GOOD");
        break;

        case AQI_MODERATE:
          display.drawBitmap(48, 0, AQIicon_moderate, 32, 32, WHITE);
          display.setCursor(17, 42);
          display.print("MODERATE");
        break;

        case AQI_SLIGHTLY_UNHEALTHY:
          display.drawBitmap(48, 0, AQIicon_slightly_unhealthy, 32, 32, WHITE);
          display.setCursor(17, 34);
          display.print("SLIGHTLY");
          display.setCursor(11, 50);
          display.println("UNHEALTHY");
        break;

        case AQI_UNHEALTHY:
          display.drawBitmap(48, 0, AQIicon_unhealthy, 32, 32, WHITE);
          display.setCursor(11, 42);
          display.print("UNHEALTHY");
        break;

        case AQI_VERY_UNHEALTHY:
          display.drawBitmap(48, 0, AQIicon_very_unhealthy, 32, 32, WHITE);
          display.setCursor(41, 34);
          display.println("VERY");
          display.setCursor(11, 50);
          display.println("UNHEALTHY");
        break;

        case AQI_HAZARDOUS:
          display.drawBitmap(48, 0, AQIicon_hazardous, 32, 32, WHITE);
          display.setCursor(11, 42);
          display.print("HAZARDOUS");
        break;
      }

      if (AQIvalue != UINT16_MAX)
      {
        display.setTextSize(1);
        display.setCursor(110, 0);
        // this is all an incredibly lazy way to right justify the text
        temp[0] = ' '; temp[1] = ' ';
        if (AQIvalue >= 100) sprintf(temp, "%3d", AQIvalue);
        else if (AQIvalue >= 10) sprintf(&temp[1], "%2d", AQIvalue);
        else sprintf(&temp[2], "%1d", AQIvalue);
        display.print(temp);

        if (AQI > AQI_GOOD)
        {
          switch (AQIpollutant)
          {
            case AQI_POLLUTANT_PM25:
              display.setCursor(98, 10);
              display.print("PM2.5");
            break;

            case AQI_POLLUTANT_PM10:
              display.setCursor(104, 10);
              display.print("PM10");
            break;

            case AQI_POLLUTANT_VOC:
              display.setCursor(110, 10);
              display.print("VOC");
            break;

            case AQI_POLLUTANT_CO2:
              display.setCursor(110, 10);
              display.print("CO2");
            break;

            case AQI_POLLUTANT_NA:
            break;
          }
        }
      }
    break;
  }

  display.display();
}

void MQTT_update()
{
  String stateTopic;
  char valueStr[6];

  if (temperature != -1)
  {
    stateTopic = "enviro/" + sensorName + "/temperature";
    dtostrf(temperature, 5, 1, valueStr);
    mqtt.publish(stateTopic.c_str(), valueStr, true);
  }

  if (humidity != -1)
  {
    stateTopic = "enviro/" + sensorName + "/humidity";
    dtostrf(humidity, 5, 1, valueStr);
    mqtt.publish(stateTopic.c_str(), valueStr, true);
  }

  if (pressure != -1)
  {
    stateTopic = "enviro/" + sensorName + "/pressure";
    dtostrf(pressure, 5, 2, valueStr);
    mqtt.publish(stateTopic.c_str(), valueStr, true);
  }

  if (PM25 != -1)
  {
    stateTopic = "enviro/" + sensorName + "/PM2.5";
    dtostrf(PM25, 5, 1, valueStr);
    mqtt.publish(stateTopic.c_str(), valueStr, true);
  }

  if (PM10 != -1)
  {
    stateTopic = "enviro/" + sensorName + "/PM10";
    dtostrf(PM10, 5, 1, valueStr);
    mqtt.publish(stateTopic.c_str(), valueStr, true);
  }

  if (CO2 != UINT16_MAX)
  {
    stateTopic = "enviro/" + sensorName + "/CO2";
    itoa(CO2, valueStr, 10);
    mqtt.publish(stateTopic.c_str(), valueStr, true);
  }

  if (VOC != UINT16_MAX)
  {
    stateTopic = "enviro/" + sensorName + "/VOC";
    itoa(VOC, valueStr, 10);
    mqtt.publish(stateTopic.c_str(), valueStr, true);
  }

  stateTopic = "enviro/" + sensorName + "/AQI/value";
  if (AQIvalue != UINT16_MAX)
  {
    itoa(AQIvalue, valueStr, 10);
    mqtt.publish(stateTopic.c_str(), valueStr, true);
  }
  else mqtt.publish(stateTopic.c_str(), "N/A", true);

  stateTopic = "enviro/" + sensorName + "/AQI/text";
  switch (AQI)
  {
    case AQI_NA:                  mqtt.publish(stateTopic.c_str(), "N/A", true); break;
    case AQI_GOOD:                mqtt.publish(stateTopic.c_str(), "Good", true); break;
    case AQI_MODERATE:            mqtt.publish(stateTopic.c_str(), "Moderate", true); break;
    case AQI_SLIGHTLY_UNHEALTHY:  mqtt.publish(stateTopic.c_str(), "Slightly Unhealthy", true); break;
    case AQI_UNHEALTHY:           mqtt.publish(stateTopic.c_str(), "Unhealthy", true); break;
    case AQI_VERY_UNHEALTHY:      mqtt.publish(stateTopic.c_str(), "Very Unhealthy", true); break;
    case AQI_HAZARDOUS:           mqtt.publish(stateTopic.c_str(), "Hazardous", true); break;
  }

  stateTopic = "enviro/" + sensorName + "/AQI/pollutant";
  if (AQI > AQI_GOOD)
  {
    switch (AQIpollutant)
    {
      case AQI_POLLUTANT_NA:    mqtt.publish(stateTopic.c_str(), "N/A", true); break;
      case AQI_POLLUTANT_PM25:  mqtt.publish(stateTopic.c_str(), "PM2.5", true); break;
      case AQI_POLLUTANT_PM10:  mqtt.publish(stateTopic.c_str(), "PM10", true); break;
      case AQI_POLLUTANT_VOC:   mqtt.publish(stateTopic.c_str(), "VOC", true); break;
      case AQI_POLLUTANT_CO2:   mqtt.publish(stateTopic.c_str(), "CO2", true); break;
    }
  }
  else mqtt.publish(stateTopic.c_str(), "N/A", true);
}

void MQTT_haDiscovery(bool enabled)
{
  String id;
  String dev;
  String topic;

  char buf[7];
  uint8_t macAddr[6];
  WiFi.macAddress(macAddr);
  sprintf(buf, "%02x%02x%02x", macAddr[3], macAddr[4], macAddr[5]);
  id = buf;

  dev = "\"dev\": { \"ids\": \"" + id + "\", \"name\": \"Enviro Sensor\", \"mdl\": \"Enviro Sensor\", \"mf\": \"Akudaikon\" }";

  if (enabled)
  {
    String commandTopic;
    String stateTopic;
    String config;
    String availTopic;

    availTopic = "enviro/" + sensorName + "/availability";

    topic = "homeassistant/sensor/enviro_" + id + "_temperature/config";
    stateTopic = "enviro/" + sensorName + "/temperature";
    config = "{\"dev_cla\": \"temperature\", \"name\": \"Temperature\", \"obj_id\": \"" + sensorName + "_temperature\", \"ic\": \"mdi:thermometer\", \"stat_t\": \"" + stateTopic + "\", \"unit_of_meas\": \"°F\", \"avty_t\": \"" + availTopic + "\", \"uniq_id\": \"" + id + "-temp\", " + dev + " }";
    mqtt.publish(topic.c_str(), config.c_str(), true);

    topic = "homeassistant/sensor/enviro_" + id + "_humidity/config";
    stateTopic = "enviro/" + sensorName + "/humidity";
    config = "{\"dev_cla\": \"humidity\", \"name\": \"Humidity\", \"obj_id\": \"" + sensorName + "_humidity\", \"ic\": \"mdi:water-percent\", \"stat_t\": \"" + stateTopic + "\", \"unit_of_meas\": \"%\", \"avty_t\": \"" + availTopic + "\", \"uniq_id\": \"" + id + "-humid\", " + dev + " }";
    mqtt.publish(topic.c_str(), config.c_str(), true);

    topic = "homeassistant/sensor/enviro_" + id + "_pressure/config";
    stateTopic = "enviro/" + sensorName + "/pressure";
    config = "{\"dev_cla\": \"pressure\", \"name\": \"Pressure\", \"obj_id\": \"" + sensorName + "_pressure\", \"ic\": \"mdi:gauge\", \"stat_t\": \"" + stateTopic + "\", \"unit_of_meas\": \"inHg\", \"avty_t\": \"" + availTopic + "\", \"uniq_id\": \"" + id + "-press\", " + dev + " }";
    mqtt.publish(topic.c_str(), config.c_str(), true);

    topic = "homeassistant/sensor/enviro_" + id + "_PM25/config";
    stateTopic = "enviro/" + sensorName + "/PM2.5";
    config = "{\"dev_cla\": \"pm25\", \"name\": \"PM2.5\", \"obj_id\": \"" + sensorName + "_pm25\", \"ic\": \"mdi:blur-linear\", \"stat_t\": \"" + stateTopic + "\", \"unit_of_meas\": \"μg/m³\", \"avty_t\": \"" + availTopic + "\", \"uniq_id\": \"" + id + "-pm25\", " + dev + " }";
    mqtt.publish(topic.c_str(), config.c_str(), true);

    topic = "homeassistant/sensor/enviro_" + id + "_PM10/config";
    stateTopic = "enviro/" + sensorName + "/PM10";
    config = "{\"dev_cla\": \"pm10\", \"name\": \"PM10\", \"obj_id\": \"" + sensorName + "_pm10\", \"ic\": \"mdi:blur-linear\", \"stat_t\": \"" + stateTopic + "\", \"unit_of_meas\": \"μg/m³\", \"avty_t\": \"" + availTopic + "\", \"uniq_id\": \"" + id + "-pm10\", " + dev + " }";
    mqtt.publish(topic.c_str(), config.c_str(), true);

    topic = "homeassistant/sensor/enviro_" + id + "_CO2/config";
    stateTopic = "enviro/" + sensorName + "/CO2";
    config = "{\"dev_cla\": \"carbon_dioxide\", \"name\": \"CO2\", \"obj_id\": \"" + sensorName + "_co2\", \"ic\": \"mdi:molecule-co2\", \"stat_t\": \"" + stateTopic + "\", \"unit_of_meas\": \"ppm\", \"avty_t\": \"" + availTopic + "\", \"uniq_id\": \"" + id + "-co2\", " + dev + " }";
    mqtt.publish(topic.c_str(), config.c_str(), true);

    topic = "homeassistant/sensor/enviro_" + id + "_VOC/config";
    stateTopic = "enviro/" + sensorName + "/VOC";
    config = "{\"dev_cla\": \"volatile_organic_compounds\", \"name\": \"VOC\", \"obj_id\": \"" + sensorName + "_voc\", \"ic\": \"mdi:spray\", \"stat_t\": \"" + stateTopic + "\", \"unit_of_meas\": \"%\", \"avty_t\": \"" + availTopic + "\", \"uniq_id\": \"" + id + "-voc\", " + dev + " }";
    mqtt.publish(topic.c_str(), config.c_str(), true);

    topic = "homeassistant/sensor/enviro_" + id + "_AQIvalue/config";
    stateTopic = "enviro/" + sensorName + "/AQI/value";
    config = "{\"dev_cla\": \"aqi\", \"name\": \"AQI Value\", \"obj_id\": \"" + sensorName + "_aqivalue\", \"ic\": \"mdi:poll\", \"stat_t\": \"" + stateTopic + "\", \"unit_of_meas\": \"AQI\", \"avty_t\": \"" + availTopic + "\", \"uniq_id\": \"" + id + "-aqiv\", " + dev + " }";
    mqtt.publish(topic.c_str(), config.c_str(), true);

    topic = "homeassistant/sensor/enviro_" + id + "_AQItext/config";
    stateTopic = "enviro/" + sensorName + "/AQI/text";
    config = "{\"name\": \"AQI Text\", \"obj_id\": \"" + sensorName + "_aqitext\", \"ic\": \"mdi:text-short\", \"stat_t\": \"" + stateTopic + "\", \"avty_t\": \"" + availTopic + "\", \"uniq_id\": \"" + id + "-aqit\", " + dev + " }";
    mqtt.publish(topic.c_str(), config.c_str(), true);

    topic = "homeassistant/sensor/enviro_" + id + "_AQIpollutant/config";
    stateTopic = "enviro/" + sensorName + "/AQI/pollutant";
    config = "{\"name\": \"AQI Dominant Pollutant\", \"obj_id\": \"" + sensorName + "_aqipollutant\", \"ic\": \"mdi:smoke\", \"stat_t\": \"" + stateTopic + "\", \"avty_t\": \"" + availTopic + "\", \"uniq_id\": \"" + id + "-aqip\", " + dev + " }";
    mqtt.publish(topic.c_str(), config.c_str(), true);

    topic = "homeassistant/switch/enviro_" + id + "_display/config";
    commandTopic = "enviro/" + sensorName + "/display/command";
    stateTopic = "enviro/" + sensorName + "/display/state";
    config = "{\"name\": \"Enviro Display\", \"obj_id\": \"" + sensorName + "_enviro_display\", \"icon\": \"mdi:lightbulb-on-outline\", \"cmd_t\": \"" + commandTopic + "\", \"stat_t\": \"" + stateTopic + "\", \"avty_t\": \"" + availTopic + "\", \"uniq_id\": \"" + id + "-switch\", " + dev + " }";
    mqtt.publish(topic.c_str(), config.c_str(), true);
  }
  else
  {
    topic = "homeassistant/sensor/enviro_" + id + "_temperature/config";
    mqtt.publish(topic.c_str(), "");

    topic = "homeassistant/sensor/enviro_" + id + "_humidity/config";
    mqtt.publish(topic.c_str(), "");

    topic = "homeassistant/sensor/enviro_" + id + "_pressure/config";
    mqtt.publish(topic.c_str(), "");

    topic = "homeassistant/sensor/enviro_" + id + "_PM25/config";
    mqtt.publish(topic.c_str(), "");

    topic = "homeassistant/sensor/enviro_" + id + "_PM10/config";
    mqtt.publish(topic.c_str(), "");

    topic = "homeassistant/sensor/enviro_" + id + "_CO2/config";
    mqtt.publish(topic.c_str(), "");

    topic = "homeassistant/sensor/enviro_" + id + "_VOC/config";
    mqtt.publish(topic.c_str(), "");

    topic = "homeassistant/sensor/enviro_" + id + "_AQIvalue/config";
    mqtt.publish(topic.c_str(), "");

    topic = "homeassistant/sensor/enviro_" + id + "_AQItext/config";
    mqtt.publish(topic.c_str(), "");

    topic = "homeassistant/sensor/enviro_" + id + "_AQIpollutant/config";
    mqtt.publish(topic.c_str(), "");

    topic = "homeassistant/switch/enviro_" + id + "_display/config";
    mqtt.publish(topic.c_str(), "");
  }
}

void MQTT_reconnect()
{
  static long lastReconnect = 0;

  if (millis() - lastReconnect > 5000)
  {
    String topic;
    String availTopic;

    availTopic = "enviro/" + sensorName + "/availability";

    if (mqtt.connect(mdnsName.c_str(), mqttUser.c_str(), mqttPassword.c_str(), availTopic.c_str(), 1, true, "offline"))
    {
      mqtt.publish(availTopic.c_str(), "online", true);

      topic = "enviro/" + sensorName + "/display/state";
      if (displayEnabled) mqtt.publish(topic.c_str(), "ON", true);
      else mqtt.publish(topic.c_str(), "OFF", true);

      topic = "enviro/" + sensorName + "/display/command";
      mqtt.subscribe(topic.c_str());

      if (haDiscovery) MQTT_haDiscovery(true);

      lastReconnect = 0;
    }
    else lastReconnect = millis();
  }
}

void MQTT_callback(char* topic, byte* payload, unsigned int length)
{
  String stateTopic = "enviro/" + sensorName + "/display/state";
  if (!strncasecmp_P((char*)payload, "on", length))
  {
    displayEnabled = true;
    display_update();
    aqiLED.SetPixelColor(0, HtmlColor(AQIColors[AQI]));
    aqiLED.Show();
    mqtt.publish(stateTopic.c_str(), "ON", true);
  }
  else if (!strncasecmp_P((char*)payload, "off", length))
  {
    displayEnabled = false;
    display.clearDisplay();
    display.display();
    aqiLED.ClearTo(RgbColor(0, 0, 0));
    aqiLED.Show();
    mqtt.publish(stateTopic.c_str(), "OFF", true);
  }
}

void EEPROM_initSettings()
{
  EEPROM.write(SETTING_INITIALIZED, SETTINGS_REV);  // EEPROM initialized
  EEPROM.write(SETTING_UPDATE_RATE, 5);             // 5 minute update rate
  EEPROM.write(SETTING_HA_DISCOVERY, 0);            // HA discovery disabled
  EEPROM.write(SETTING_AQILED_BRIGHTNESS, 100);     // AQI LED brightness 100%
  EEPROM.write(SETTING_DELIM, 0);  // Dummy setting to delimit start of strings
  EEPROM_writeString(SETTING_NAME, SETTING_NAME_LENGTH, mdnsName);
  EEPROM_writeString(SETTING_MDNS, SETTING_MDNS_LENGTH, mdnsName);
  EEPROM_writeString(SETTING_MQTT_SERVER, SETTING_MQTT_SERVER_LENGTH, "");
  EEPROM_writeString(SETTING_MQTT_USER, SETTING_MQTT_USER_LENGTH, "");
  EEPROM_writeString(SETTING_MQTT_PASSWORD, SETTING_MQTT_PASSWORD_LENGTH, "");
  EEPROM_writeFloat(SETTING_TEMP_OFFSET, 0);
  EEPROM.commit();
}

void EEPROM_readSettings()
{
  if (EEPROM.read(SETTING_INITIALIZED) != SETTINGS_REV) EEPROM_initSettings();

  updateRate = EEPROM.read(SETTING_UPDATE_RATE);
  haDiscovery = EEPROM.read(SETTING_HA_DISCOVERY);
  aqiLEDBrightness = EEPROM.read(SETTING_AQILED_BRIGHTNESS);
  tempOffset = EEPROM_readFloat(SETTING_TEMP_OFFSET);
  sensorName = EEPROM_readString(SETTING_NAME, SETTING_NAME_LENGTH);
  mdnsName = EEPROM_readString(SETTING_MDNS, SETTING_MDNS_LENGTH);
  mqttServer = EEPROM_readString(SETTING_MQTT_SERVER, SETTING_MQTT_SERVER_LENGTH);
  mqttUser = EEPROM_readString(SETTING_MQTT_USER, SETTING_MQTT_USER_LENGTH);
  mqttPassword = EEPROM_readString(SETTING_MQTT_PASSWORD, SETTING_MQTT_PASSWORD_LENGTH);
}

String EEPROM_readString(uint16_t startAddress, uint16_t lengthAddress)
{
  String returnString = "";
  uint8_t length = constrain(EEPROM.read(lengthAddress), 0, MAX_STRING_LENGTH);
  for (int i = 0; i < length; i++) returnString += (char)EEPROM.read(startAddress + i);
  return returnString;
}

void EEPROM_writeString(uint16_t startAddress, uint16_t lengthAddress, String str)
{
  uint8_t length = constrain(str.length(), 0, MAX_STRING_LENGTH);
  EEPROM.write(lengthAddress, length);
  for (int i = 0; i < length; i++) EEPROM.write(startAddress + i, str[i]);
  EEPROM.commit();
}

float EEPROM_readFloat(uint16_t startAddress)
{
  float value = 0.0;
  byte* p = (byte*)(void*)&value;
  for (uint8_t i = 0; i < sizeof(value); i++) *p++ = EEPROM.read(startAddress + i);
  return value;
}

void EEPROM_writeFloat(uint16_t startAddress, float value)
{
  byte* p = (byte*)(void*)&value;
  for (uint8_t i = 0; i < sizeof(value); i++) EEPROM.write(startAddress + i, *p++);
}

void settings_get()
{
  DynamicJsonDocument root(256);

  root["sensorName"] = sensorName;
  root["mdnsName"] = mdnsName;
  root["mqttServer"] = mqttServer;
  root["mqttUser"] = mqttUser;
  root["mqttPassword"] = mqttPassword;
  root["updateRate"] = updateRate;
  root["mqttServer"] = mqttServer;
  root["tempOffset"] = tempOffset;
  root["haDiscovery"] = haDiscovery;
  root["aqiLEDBrightness"] = aqiLEDBrightness;

  String buffer;
  serializeJson(root, buffer);

  server.sendHeader("Cache-Control", "no-cache");
  server.send(200, "text/json", buffer);
}

void settings_save()
{
  String response;

  if (server.hasArg("sensorName"))
  {
    if (server.arg("sensorName") != "")
    {
      String temp = server.arg("sensorName");
      if (haDiscovery)
      {
        // If sensor name changed, we need to wipe existing HA discovery topics that have old name
        // Otherwise they'll end up orphaned
        if (strcmp(temp.c_str(), sensorName.c_str()) != 0) MQTT_haDiscovery(false);
      }
      sensorName = temp;
      EEPROM_writeString(SETTING_NAME, SETTING_NAME_LENGTH, sensorName);
    }
  }
  if (server.hasArg("mdnsName"))
  {
    mdnsName = server.arg("mdnsName");
    EEPROM_writeString(SETTING_MDNS, SETTING_MDNS_LENGTH, mdnsName);
  }
  if (server.hasArg("mqttServer"))
  {
    mqttServer = server.arg("mqttServer");
    EEPROM_writeString(SETTING_MQTT_SERVER, SETTING_MQTT_SERVER_LENGTH, mqttServer);
  }
  if (server.hasArg("mqttUser"))
  {
    mqttUser = server.arg("mqttUser");
    EEPROM_writeString(SETTING_MQTT_USER, SETTING_MQTT_USER_LENGTH, mqttUser);
  }
  if (server.hasArg("mqttPassword"))
  {
    mqttPassword = server.arg("mqttPassword");
    EEPROM_writeString(SETTING_MQTT_PASSWORD, SETTING_MQTT_PASSWORD_LENGTH, mqttPassword);
  }
  if (server.hasArg("haDiscovery"))
  {
    bool temp = (server.arg("haDiscovery") == "on" ? true : false);
    if (haDiscovery && !temp) MQTT_haDiscovery(false);
    haDiscovery = temp;
    EEPROM.write(SETTING_HA_DISCOVERY, haDiscovery);
  }
  if (server.hasArg("updateRate"))
  {
    int temp = server.arg("updateRate").toInt();
    if (temp > 0)
    {
      updateRate = temp;
      EEPROM.write(SETTING_UPDATE_RATE, updateRate);
    }
  }
  if (server.hasArg("aqiLEDBrightness"))
  {
    int temp = server.arg("aqiLEDBrightness").toInt();
    if (temp >= 0 && temp <= 100)
    {
      aqiLEDBrightness = temp;
      EEPROM.write(SETTING_AQILED_BRIGHTNESS, aqiLEDBrightness);
    }
  }
  if (server.hasArg("tempOffset"))
  {
    tempOffset = server.arg("tempOffset").toFloat();
    EEPROM_writeFloat(SETTING_TEMP_OFFSET, tempOffset);
  }

  EEPROM.commit();

  display.clearDisplay();

  // draw lazy checkmark
  display.setTextSize(2);
  display.setCursor(57, 5);
  display.print("\\");
  display.setCursor(65, 5);
  display.print("/");
  display.fillRect(57, 5, 4, 11, BLACK);

  display.setTextSize(1);
  display.setCursor(18, 25);
  display.print(F("Settings Updated!"));
  display.setCursor(31, 40);
  display.print(F("Restarting..."));
  display.display();

  response = F("<META http-equiv=\"refresh\" content=\"15;URL='/'\">Settings saved! Restarting to take effect...<br><br>");
  server.send(200, "text/html", response);
  delay(3000);
  ESP.reset();
}

bool handleFileRead(String path)
{
  if (path.endsWith("/")) path += "index.htm";

  String dataType = "text/plain";
  if (path.endsWith(".htm")) dataType = "text/html";
  else if (path.endsWith(".css")) dataType = "text/css";
  else if (path.endsWith(".js")) dataType = "text/javascript";

  if (SPIFFS.exists(path))
  {
    File file = SPIFFS.open(path, "r");
    server.streamFile(file, dataType);
    file.close();
    return true;
  }
  return false;
}

void BME680_loadState(void)
{
  if (EEPROM.read(SETTING_BSEC_DATA) == BSEC_MAX_STATE_BLOB_SIZE)
  {
    // Read existing state in EEPROM
    for (uint16_t i = 0; i < BSEC_MAX_STATE_BLOB_SIZE; i++) bsecState[i] = EEPROM.read(SETTING_BSEC_DATA + i + 1);
    bmeSensor.setState(bsecState);
  }
  else
  {
    // Erase the EEPROM state data with zeroes
    for (uint16_t i = SETTING_BSEC_DATA; i < BSEC_MAX_STATE_BLOB_SIZE + 1; i++) EEPROM.write(i, 0);
    EEPROM.commit();
  }
}

void BME680_updateState(void)
{
  bool update = false;
  if (stateUpdateCounter == 0)
  {
    // First state update when IAQ accuracy is >= 3
    if (bmeSensor.staticIaqAccuracy >= 3)
    {
      update = true;
      stateUpdateCounter++;
    }
  }
  else
  {
    // Update every BSEC_STATE_SAVE_PERIOD minutes
    if ((stateUpdateCounter * BSEC_STATE_SAVE_PERIOD) < millis())
    {
      update = true;
      stateUpdateCounter++;
    }
  }

  if (update)
  {
    bmeSensor.getState(bsecState);
    for (uint16_t i = 0; i < BSEC_MAX_STATE_BLOB_SIZE ; i++) EEPROM.write(SETTING_BSEC_DATA + i + 1, bsecState[i]);
    EEPROM.write(SETTING_BSEC_DATA, BSEC_MAX_STATE_BLOB_SIZE);
    EEPROM.commit();
  }
}

float mapFloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
